﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SOInvader.Managers
{
	/// <summary>
	/// Extends the <code>SceneController</code> to provide game scene
	/// control for the application.
	/// </summary>
	public class GameSceneController : SceneController 
	{
		#region Abstract Base Implementation

		/// <summary>
		/// Loads the Intro scene
		/// </summary>
		public override void LoadNextScene()
		{
			LoadNextScene (SceneNameIntro);
		}

		public override string MyScene ()
		{
			return SceneNameGame;
		}

		#endregion
	}
}