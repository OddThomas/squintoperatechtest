﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SOInvader.Managers
{
	public class ConfigSceneController : SceneController 
	{
		#region MonoBehaviour Implementation

		void Start()
		{
			LoadNextScene ();
		}

		#endregion


		#region Abstract Base Implementation

		/// <summary>
		/// Loads the Intro scene
		/// </summary>
		public override void LoadNextScene()
		{
			LoadNextScene (SceneNameIntro);
		}

		public override string MyScene ()
		{
			return SceneNameConfig;
		}

		#endregion
	}
}