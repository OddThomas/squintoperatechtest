﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SOInvader.Managers
{
	/// <summary>
	/// Extends the <code>SceneController</code> to provide introduction scene
	/// control for the application.
	/// </summary>
	public class IntroSceneController : SceneController 
	{
		#region Configuration Properties

		[Tooltip("Set the UI gameobject showing the 'Loading' overlay")]
		[SerializeField] GameObject loadingOverlay;

		#endregion


		#region MonoBehaviour Implementation

		void Awake()
		{
			if (loadingOverlay == null) {
				Debug.LogError ("<Code=red><a>Missing</a></Code> loading overlay panel reference for IntroSceneController", this);
				return;
			}

			loadingOverlay.SetActive (false);
		}

		#endregion


		#region Abstract Base Implementation

		/// <summary>
		/// Loads the Game scene
		/// </summary>
		public override void LoadNextScene()
		{
			loadingOverlay.SetActive (true);
			LoadNextScene (SceneNameGame);
		}

		public override string MyScene ()
		{
			return SceneNameIntro;
		}

		#endregion
	}
}