﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SOInvader.Managers
{
	public class GameManager : MonoBehaviour 
	{
		#region Configuration Properties



		#endregion


		#region MonoBehaviour Implementation

		/// <summary>
		/// Awake with a reference to this instance
		/// </summary>
		void Awake()
		{
			Instance = this;
			DontDestroyOnLoad (gameObject);
		}

		#endregion


		#region Public Class Methods

		/// <summary>
		/// Gets or sets the GameManager instance
		/// </summary>
		/// <value>The instance.</value>
		public static GameManager Instance { get; protected set; }

		#endregion
	}
}