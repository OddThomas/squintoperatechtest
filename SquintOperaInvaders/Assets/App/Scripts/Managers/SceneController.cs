﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SOInvader.Managers
{
	/// <summary>
	/// Provides a common base for controller classes to handle the different 
	/// scenes in the application. The term controller has been used here to 
	/// avoid conflict with the Untiy <code>SceneManager</code>.
	/// </summary>
	public abstract class SceneController : MonoBehaviour 
	{
		#region Constants

		protected const string SceneNameConfig = "Config";
		protected const string SceneNameIntro = "Intro";
		protected const string SceneNameGame = "Game";

		#endregion


		#region Abstract Definitions

		/// <summary>
		/// Simplest override is to call <code>LoadNextScene(string sceneName)</code>
		/// with the name of the next scene.
		/// </summary>
		public abstract void LoadNextScene ();

		/// <summary>
		/// Returns the string name of the controller's own scene
		/// </summary>
		/// <returns>The scene.</returns>
		public abstract string MyScene ();

		#endregion


		#region Scene Load Handling

		/// <summary>
		/// Loads the next scene with the given name
		/// </summary>
		/// <param name="sceneName">Scene name.</param>
		public void LoadNextScene(string sceneName)
		{
			StartCoroutine (LoadSceneAsync (sceneName));
		}

		/// <summary>
		/// Loads the scene asynchronously and swaps over once ready, allowing the
		/// current scene to continue operating until ready to swap.
		/// </summary>
		/// <returns>The scene async.</returns>
		/// <param name="sceneName">Scene name.</param>
		IEnumerator LoadSceneAsync(string sceneName)
		{
			AsyncOperation aop = SceneManager.LoadSceneAsync (sceneName, LoadSceneMode.Additive);
			aop.allowSceneActivation = false;
			while (aop.progress < 0.9f)
				yield return null;
			aop.allowSceneActivation = true;

			Scene nextScene = SceneManager.GetSceneByName (sceneName);
			SceneManager.SetActiveScene (nextScene);
			SceneManager.UnloadSceneAsync (MyScene());
		}

		#endregion
	}
}
