# Squint/Opera Technical Test

## Game Overview

The purpose of the task was to create a Space Invaders game with a twist.  My thoughts here were to create a game that would work with standard input on a PC, but which could be configured for mobile AR/VR, allowing users to see the descending aliens either in a virtual world on the device or overlaid over the real world.

## Game Mechanics

### Alien Mechanics

There would be two levels of alien attack:

1.  Space-based by craft descending from above, firing at weapons bases used by the player.
2.  Ground-based aliens deployed from craft reaching a certain altitude.

Once all of the troops deployed by a craft have been exhausted, they continue to descend until they hit the ground or get destroyed.

Each wave shall require more fire-power and tactics than the previous wave.

### Player Mechanics

Players would gain points through:

1.  Successful destruction of the alien craft.
2.  Successful destruction of aliens on the ground.

Players would gain health through:

1.  Successful destruction of sufficient numbers of aliens on the ground.

Players would lose health through:

1.  Direct and successful attacks from ground-based aliens.

Players would gain power ups (improved fire-power) through:

1.  Successful destruction of complete waves of alien craft.
2.  Successful destruction of sufficient ground-based aliens.

## Analytics

Although Unity provides analytics integration, this does not necessarily allow the client to access the data, as it is presented as part of the developer console.  There is a wide variety of analytics services available which can be access via a REST HTTP API, so the client should be able to choose the service that provides the services for their budget. However, if these are too expensive, the simplest option is to create a secure web service that accepts JSON data and allows the user to download the tracking data for a period, to analyse as they see fit.

Because it may not always be possible to submit tracking data, the application should have both a persistence layer and a queueing system.  The SimpleSQL API will provide the persistence layer across both standalone and mobile platforms, allowing tracking data to be submitted to the database should the queue need persisting (such as when the application is closed).  When the app is re-opened, the queue can be re-populated from the database table and submission can re-commence. 
